#pragma once

#include "aux.hpp"

namespace fowl
{
namespace components
{

struct OrthogonalCamera
{
    sf::Vector2f position, size;
    float zoom = 1.f;
    entity::EntityID tracking = 0;

    void track (entity::EntityID entity)
    {
        tracking = entity;
    }

    template<typename CS>
    void update_target (entity::EM<CS>* em)
    {
        if(tracking)
        {
        }
    }

    b2AABB aabb()
    {
        auto sz = size / 2.f * zoom;
        b2AABB bb;
        bb.lowerBound = b2vec2((position - sz) / B2Ratio);
        //     (position.x - sz.x) / B2Ratio,
        //     (position.y - sz.y) / B2Ratio
        // );
        bb.upperBound = b2vec2((position + sz) / B2Ratio);
        //     (position.x + sz.x) / B2Ratio,
        //     (position.y + sz.y) / B2Ratio
        // );
        assert(bb.IsValid());
        return bb;
    }
};


struct Material
{
    sf::Color
        fill_color = sf::Color::White,
        outline_color = sf::Color::Transparent;
    sf::Texture* tex = nullptr;

};


}
}
