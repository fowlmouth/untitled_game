#pragma once


namespace fowl
{
namespace components
{
struct World
{
    b2World* world;

    struct RayCastResult
    {
        b2Vec2 a, b;

    };
    std::vector<RayCastResult> raycasts;
    bool draw_raycasts;


    World()
    {
        world = new b2World(b2Vec2(0,0));
    }
    ~World()
    {
        delete world;
    }
};

struct Body
{
    b2Body* body = 0;

    ~Body()
    {
        if(body) body->GetWorld()->DestroyBody(body);
    }
};

struct Shape
{
    b2Fixture* fixture = nullptr;

    void clear_fixture()
    {
        if(fixture)
        {
            fixture->GetBody()->DestroyFixture(fixture);
            fixture = nullptr;
        }
    }
    ~Shape()
    {
        clear_fixture();
    }

};
}




namespace entity
{


DefineBehavior(components::World)
{
    void raycast_request(b2RayCastCallback *callback, const b2Vec2 &point1, const b2Vec2 &point2)
    {
    }
};

DefineBehavior(components::Body)
{
    void create_body (b2BodyDef& body_def)
    {
        auto& self = this->derived();
        Assert(self.is_valid(), "invalid entity");
        auto world = self.template search_parents<components::World>();
        Assert(world.get(), "world not found");
        Assert(world->world, "world not initialized");
        if(self->body)
            self->body->GetWorld()->DestroyBody(self->body);
        self->body = world->world->CreateBody(&body_def);
    }
    void create_dynamic (const b2Vec2& pos)
    {
        b2BodyDef bd;
        bd.type = b2_dynamicBody;
        bd.position = pos;
        create_body(bd);
    }

    void create_static ()
    {
        b2BodyDef bd;
        bd.type = b2_staticBody;
        create_body(bd);
    }
};
DefineBehavior(components::Shape)
{

    void create_fixture (b2FixtureDef& fixtureDef)
    {
        auto& self = this->derived();
        assert(self.is_valid());

        auto body = self.search_parents< components::Body >();
        assert(body.get());
        assert(body->body);

        self->clear_fixture();

        fixtureDef.userData = (void*)self.id;
        self->fixture = body->body->CreateFixture(&fixtureDef);
    }

    void create_circle(float radius, b2Vec2 offset,
            float density = 0.5,
            float friction = 0.01)
    {
        b2CircleShape shape;
        shape.m_p = offset;
        shape.m_radius = radius/B2Ratio;
        b2FixtureDef fd;
        fd.density = density;
        fd.friction = friction;
        fd.shape = &shape;

        create_fixture(fd);

    }


    void create_box (float w, float h, b2Vec2 offset,
        float density = 0.5,
        float friction = 0.01)
    {
        b2PolygonShape shape;
        shape.SetAsBox(w/B2Ratio, h/B2Ratio);
        b2FixtureDef fd;
        fd.density = density;
        fd.friction = friction;
        fd.shape = &shape;

        create_fixture(fd);

    }

    void create_from_points (const auto& points,
        float density = 0.5, float friction = 0.01)
    {
        b2PolygonShape polygonShape;
        polygonShape.Set(&points[0], points.size());

        b2FixtureDef fd;
        fd.density = density;
        fd.friction = friction;
        fd.shape = &polygonShape;
        create_fixture(fd);
    }

    void create_radial (float radius, int parts = 6,
        float density = 0.5,
        float friction = 0.01)
    {
        b2PolygonShape polygonShape;
        b2Vec2 vertices[parts];
        radius /= B2Ratio;
        for (int i = 0; i < parts; i++) {
          float angle = (float)i/parts * M_PI * 2;
          vertices[i].Set(//cos(angle), sin(angle));//
            sin(angle), cos(angle));
          vertices[i] *= radius;
        }
        polygonShape.Set(vertices, parts);
        b2FixtureDef fd;
        fd.density = density;
        fd.friction = friction;
        fd.shape = &polygonShape;

        create_fixture(fd);
    }

    void create_edge (b2Vec2 p1, b2Vec2 p2,
        float friction = 0.5,
        float restitution = 0.9)
    {

        b2EdgeShape shape;
        shape.m_vertex1 = p1;
        shape.m_vertex2 = p2;
        b2FixtureDef fd;
        fd.density = 0.5;
        fd.friction = friction;
        fd.restitution = restitution;
        fd.shape = &shape;

        create_fixture(fd);

    }
};
}

}