#pragma once

#include <algorithm>
#include <type_traits>
#include <cstdlib>
#include <cxxabi.h>

#include "typeset.hpp"
#include "events.hpp"





namespace fowl
{
namespace entity
{

using EntityID = std::size_t;
using ComponentID = unsigned int;

struct DefaultTypeSet_;
using DefaultTypeSet = fowl::TypeSet<DefaultTypeSet_>;


struct BaseAllocator
{
    virtual void* alloc () = 0;
    virtual void dealloc(void*) = 0;
};
template<typename T>
struct AllocateComponent: BaseAllocator
{
    void* alloc () { return (void*)new T; }
    void dealloc(void* ptr) { delete (T*)ptr; }
};

template<typename T, typename Derived>
struct ComponentBehavior
{
};

template<typename EntityType, typename T>
struct ComponentHandle
: public ComponentBehavior< T, ComponentHandle<EntityType,T> >,
  public EntityType
{
    using Entity = EntityType;
    using type = T;
    
    T* data = nullptr;

    inline ComponentHandle(Entity entity, T* data)
    : Entity(entity), data(data)
    {}

    inline T* operator->() { return data; }
    inline T& operator* () { return *data; }

    inline T* get() { return data; }
    //inline operator Entity() { return entity; }

    inline bool is_valid();
    inline operator bool() { return is_valid(); }
};

using ComponentMask = std::bitset<MaxComponents>;


template<typename Set, typename...>
struct set_filters_impl;

template<typename Set, typename Co, typename... Cos>
struct set_filters_impl<Set,Co,Cos...>
{
    inline static void set(ComponentMask& mask)
    {
        mask.set(Set::template get_id<Co>());
        set_filters_impl<Set, Cos...>::set(mask);
    }
};

template<typename Set>
struct set_filters_impl<Set>
{
    inline static void set(ComponentMask& mask)
    {
    }
};

template<typename Set, typename... Cos>
inline static void set_filters(ComponentMask& mask)
{
    set_filters_impl<Set, Cos...>::set(mask);
}




template<typename CS = DefaultTypeSet>
struct E;

template<typename CS = DefaultTypeSet>
struct EM;

template<typename CS = DefaultTypeSet>
struct BaseSystem
{
    using EntityManager = EM<CS>;
    EntityManager* em;
    virtual ~BaseSystem() {}
    virtual void update (float dt) = 0;
};

template<typename CS>
struct EM
{
    using Entity = E<CS>;
    using BaseSys = BaseSystem<CS>;

    std::vector<BaseAllocator*> allocators;
    std::vector<BaseSys*> systems;
    template<typename T>
    using ComponentHandle = ComponentHandle<Entity, T>;

    EventSource event_source;

    struct EntityData
    {
        bool valid = false;
        std::bitset<MaxComponents> components;
        EntityID self = 0, parent = 0;
        std::vector<EntityID> children;
        std::vector<void*> data;
        EventSubscriber* dispatcher = nullptr;

    };
    std::vector<EntityData> entities;


    template<typename T>
    T* alloc()
    {
        const auto id = CS::template get_id<T>(); //getComponentID<T>::id;
        if(allocators.size() < id+1)
            allocators.resize(CS::num_types(), nullptr);
        if(!allocators[id])
            allocators[id] = new AllocateComponent<T>;
        return (T*)(allocators[id]->alloc());
    }

    EntityID next_ = 0;
    EntityID next_id() { return ++next_; }

    Entity create_entity(EntityID parent);
    void destroy_entity(EntityID id);

    Entity get (EntityID entity);

    bool is_valid (EntityID id)
    {
        //PrintCode(id);
        return id < entities.size() && entities[id].valid;
    }
    bool has_component(EntityID id, ComponentID component)
    {
        //PrintCode(id << "," << component);
        return entities[id].components[component];
    }

    template<typename T>
    ComponentHandle<T> create_component(EntityID entity)
    {
        assert(is_valid(entity));
        ComponentID comp = CS::template get_id<T>();//getComponentID<T>::id;

        if(has_component(entity, comp))
        {
            return get_component<T>(entity);
        }
        //assert(! has_component(entity, comp));
        T* data = alloc<T>();

        auto& storage = entities[entity];
        if(storage.data.size() < comp+1) storage.data.resize(comp+1, nullptr);
        storage.data[comp] = data;
        storage.components.set(comp, true);

        return ComponentHandle<T>(Entity{entity, this}, data);
    }

    template<typename T>
    ComponentHandle<T> get_component(EntityID entity) 
    {
        auto id = CS::template get_id<T>();
        if(entities[entity].data.size() < id+1 ||//getComponentID<T>::id+1 ||
            !entities[entity].data[id])//getComponentID<T>::id])
            return ComponentHandle<T>(Entity{entity, this}, nullptr);
        else
            return ComponentHandle<T>(Entity{entity, this}, (T*)entities[entity].data[id] );
    }

    EntityID parent (EntityID entity)
    {
        return entities[entity].parent;
    }



    BaseSys* add_system (BaseSys* system)
    {
        systems.push_back(system);
        system->em = this;
        return system;
    }

    void update_all (float dt)
    {
        for(auto& sys: systems) sys->update(dt);
    }

    template<typename...Cs>
    struct view
    {
        ComponentMask mask;
        EM<CS>* em;
        view(EM<CS>* em, ComponentMask m)
        : mask(m), em(em)
        {
        }
        void each (auto callback)
        {
            for(auto& entity: em->entities)
            {
                if(entity.valid && (entity.components & mask) == mask)
                {
                    callback(
                        Entity{entity.self, em},
                        (*em->get_component<Cs>(entity.self))...
                    );
                }
            }
        }
    };

    template<typename...Cs>
    inline void each (auto callback)
    {
        ComponentMask filter;
        set_filters<Cs...>(filter);
        view<Cs...>(this, filter).each(callback);
    }


};


template<typename CS>
struct E
{
    using Entity = E<CS>;
    using EntityManager = EM<CS>;
    template<typename T>
    using Handle = ComponentHandle<Entity, T>;

    EntityID id;
    EntityManager* m;


    Entity create_child()
    {
        return m->create_entity(id);
    }

    operator EntityID() { return id; }
    inline bool is_valid() { return m->is_valid(id); }

    template<typename T>
    Handle<T> create_component()
    {
        return create<T>();
    }

    template<typename T>
    Handle<T> create()
    {
        return m->create_component<T>(id);
    }

    template<typename T>
    Handle<T> get()
    {
        return m->template get_component<T>(id);
    }

    template<typename T>
    Handle<T> search_parents() const
    {
        EntityID e = id;
        while(e)
        {
            //Print("  " << e);
            auto handle = m->template get_component<T>(e);
            if(handle.is_valid())
            {
                //PrintCode(handle.get());
                return handle;
            }
            if(! m->is_valid(e))
            {
                Print("  (break invalid " << e);
                break;
            }

            e = m->parent(e);
        }
        return Handle<T>(Entity{0, m}, (T*)0);
    }

    const typename EntityManager::EntityData& get_data() const
    {
        return m->entities[id];
    }

    void subscribe (int event, const EventSubscriber::Callback& cb)
    {
        auto& d = m->entities[id];
        if(!d.dispatcher)
        {
            d.dispatcher = new EventSubscriber;
            m->event_source.add(d.dispatcher);
        }
        d.dispatcher->subscribe(event, cb);
    }

};


template<typename E, typename T>
bool ComponentHandle<E,T>::is_valid ()
{
    return E::is_valid() && (bool)data;
}

template<typename CS>
typename EM<CS>::Entity   EM<CS>::create_entity(EntityID parent)
{
    EntityID id = next_id();
    if(entities.size() < id+1) entities.resize(id+1, EntityData());
    if(parent) entities[parent].children.push_back(id);
    EntityData& d = entities[id];
    d.valid = true;
    d.self  = id;
    d.parent = parent;
    return Entity{id, this};
}
template<typename CS>
void EM<CS>::destroy_entity(EntityID id)
{
    if(! entities[id].valid) return;
    auto& d = entities[id];
    d.valid = false;
    if(d.parent)
    {
        auto& p = entities[d.parent ];
        //std::erase(std::remove(p.children.begin(), p.children.end(), id));
        const auto iter = std::find(p.children.begin(), p.children.end(), id);
        if(iter != p.children.end()) p.children.erase(iter);
    }
    for(auto c: d.children)
    {
        entities[c].parent = d.parent;
        if(d.parent) entities[d.parent].children.push_back(c);
    }

    d.parent = 0;
    for(int i = 0; i < d.data.size(); ++i)
        if(d.data[i])
        {
            allocators[i]->dealloc(d.data[i]);
            d.data[i] = nullptr;
        }
    d.components.reset(false);

    return;
}


template<typename CS>
typename EM<CS>::Entity     EM<CS>::get (EntityID entity)
{
    return {is_valid(entity) ? entity : 0, this};
}

template<typename Set, typename... Cs>
struct System: public BaseSystem<Set>
{
    ComponentMask filter;
    using EntityManager = typename BaseSystem<Set>::EntityManager;

    System()
    {
        set_filters<Set, Cs...>(filter);
    }

    typename EntityManager::template view<Cs...>    entities()
    {
        return typename EntityManager::template view<Cs...>(this->em, filter);
    }

    inline void each (auto callback)
    {
        entities().each(callback);
    }


};

template<typename Set>
struct Domain
{
    using Entity = entity::E<Set>;
    using EntityManager = entity::EM<Set>;
    using BaseSystem = entity::BaseSystem<Set>;
    template<typename... Cx>
    using System = entity::System<Set, Cx...>;
};

using DefaultDomain = Domain<DefaultTypeSet>;


} //entity::
} //fowl::

