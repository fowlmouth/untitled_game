#pragma once

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include "json.hpp"
#include "aux.hpp"

#define B2Ratio 32.0f

namespace fowl
{

namespace entity
{


template<typename Derived>
struct Helper
{
    inline Derived& derived() { return *static_cast<Derived*>(this); }
};

// template<typename C>
// using DefaultComponentBehavior = entity::ComponentBehavior< C, entity::Entity::Handle<C> >;

#define DefineBehavior(C) \
template<> \
struct ComponentBehavior< C, E<DefaultTypeSet>::Handle<C> > \
: Helper< E<DefaultTypeSet>::Handle<C> >


}

}


#include "components/physics.hpp"
#include "components/graphics.hpp"

namespace fowl
{
namespace components
{


using json = nlohmann::json;
struct EntityTemplate
{
    json rules;

    struct Instr
    {};
    std::vector<Instr> instrs;

    // void create(entity::EntityManager* em)
    // {
    // }
};


struct SpawnRegion
{
    sf::Vector2f upper_left, size;
    EntityTemplate tmpl;
    int create = 10;



    b2Vec2 get_b2position()
    {
        return b2vec2( (upper_left + size / 2.0f) / B2Ratio);
    }

};

struct EnergyRating
{
    float current, max;
    bool take(float amount)
    {
        if(current < amount)
            return false;
        current -= amount;
        return true;
    }
};
struct Vitality
{
    EnergyRating energy, health;

};


}

namespace entity
{
DefineBehavior(components::Vitality)
{
    bool take_energy (float energy)
    {
        return this->derived()->energy.take(energy);
    }
};

}


}



