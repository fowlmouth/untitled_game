#pragma once

std::ostream& operator<< (std::ostream& os, const b2Vec2& vec)
{
    return os << '(' << vec.x << ',' << vec.y << ')';
}

std::ostream& operator<< (std::ostream& os, const sf::Vector2f& vec)
{
    return os << '(' << vec.x << ',' << vec.y << ')';
}


namespace sf
{

class SFML_GRAPHICS_API LineShape : public Shape
{
public:

    explicit LineShape(const Vector2f& point1, const Vector2f& point2)
    : direction(point2 - point1)
    {
        setPosition(point1);
        setThickness(2.f);
    }

    void setThickness(float thickness)
    {
        this->thickness = thickness;
        update();
    }

    float getThickness() const
    {
        return thickness;
    }

    float getLength() const
    {
        return std::sqrt(direction.x * direction.x + direction.y * direction.y);
    }

    virtual std::size_t getPointCount() const
    {
        return 4;
    }

    virtual Vector2f getPoint(std::size_t index) const
    {
        Vector2f unitDirection = direction/getLength();
        Vector2f unitPerpendicular(-unitDirection.y, unitDirection.x);

        Vector2f offset = (thickness / 2.f) * unitPerpendicular;

        switch (index)
        {
            default:
            case 0: return offset;
            case 1: return (direction + offset);
            case 2: return (direction - offset);
            case 3: return (-offset);
        }
    }

private:
    Vector2f direction; ///< Direction of the line
    float thickness;    ///< Thickness of the line
};








}


void draw_line (
        sf::RenderTarget& target,
        sf::Vector2f p1, sf::Vector2f p2, 
        sf::Color fillColor,
        float thickness = 2.0)
{
    sf::LineShape shape(p1, p2);
    shape.setFillColor(fillColor);
    shape.setThickness(thickness);
    target.draw(shape);
}


inline b2Vec2 b2vec2 (const sf::Vector2f& sfVector)
{
    return b2Vec2(sfVector.x, sfVector.y);
}
inline sf::Vector2f sfVec (const b2Vec2& vector)
{
    return sf::Vector2f(vector.x, vector.y);
}


template<typename T>
T clamp(T value, T min, T max)
{
    return std::min( std::max( value, min ), max );
}

