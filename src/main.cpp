

#include <string>
#include <vector>
#include <memory>
#include <utility>
#include <cassert>
#include <random>
#include <bitset>
#include <iostream>



#define Print(S) std::cout << S << std::endl
#define ShowComponent(C) Print(#C " " << getComponentID<C>::id)
#define PrintCode(C) Print(#C " " << C)
#define PrintDbg(M) Print(M);

struct assertion_failed{
	const char *msg, *condition;
};
#define Assert(condition, msg) \
	if(!condition) throw assertion_failed{msg, #condition};


#include "brains.hpp"

const int MaxComponents = 12;
#include "entity.hpp"
#include "components.hpp"

std::random_device rd;
std::mt19937 gen(rd());

#include "systems.hpp"
#include "application.hpp"

//#include "brains_tests.hpp"


struct opt_parser
{
	std::function<void(char)> short_opt;
	std::function<void(const std::string&,const std::string&)> long_opt;
	std::function<void(const std::string&)> argument;
	void parse(int argc, char** argv)
	{
		for(int i = 1; i < argc; ++i)
		{
			char* o = argv[i];

			if(*o == '-')
			{
				if(*++o == '-')
				{
					std::string key(++o);
					std::string val(argv[++i]);
					if(long_opt) long_opt(key, val);
				}
				else if(short_opt)
				{
					++o;
					while(*o) short_opt(*o++);
				}
			}
			else if(argument)
			{
				std::string s(o);
				argument(s);
			}
		}
	}
};

//use later
sf::Font font;

int main(int argc, char** argv)
{
    int ScreenW = 800, ScreenH = 600;
	sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

    std::string scene = "Racetrack";
    opt_parser p;
    p.long_opt = [&ScreenW, &ScreenH](const std::string& key, const std::string& val){
    	if(key == "w") ScreenW = std::stoi(val);
    	else if(key == "h") ScreenH = std::stoi(val);
    };
    p.argument = [&scene](const std::string& arg){
    	scene = arg;
    };
    p.parse(argc, argv);


    sf::RenderWindow win(
        sf::VideoMode(ScreenW, ScreenH), "",
        sf::Style::Default, settings);
    win.setFramerateLimit(60);
    font.loadFromFile("Ubuntu-R.ttf");


    fowl::App app(win);
    if(scene == "Racetrack")
    	app.setup(fowl::Racetrack());
    else if(scene == "TestScene")
    	app.setup(fowl::TestScene());
    else
    	app.setup(fowl::TestScene2());
    //fowl::TestScene()); //Racetrack());
    app.run();




    return 0;
}
