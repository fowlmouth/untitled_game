#pragma once


namespace fowl
{
namespace actuators
{

    using namespace entity;

    template<typename CB>
    struct RaycastRequest
    {
        using Callback = CB;
        b2Vec2 position;
        float angle, distance;
        Callback cb;

        inline float cost() const
        {
            return .3f * distance;
        }
    };
    template<typename Callback>
    struct RaycastAction
    {
        RaycastRequest<Callback> activate(DefaultDomain::Entity obj, auto organ, auto controller) const
        {
            float magnitude = controller->parameter(0);
            //if(magnitude < 0.1) Print("really weak raycast " << magnitude << " from " << obj.id);
            return {
                organ->get_position(obj),
                organ->get_angle(obj),
                magnitude,
                Callback(static_cast<typename Callback::Organ*>(organ))
            };
        }
    };

    template<typename CB>
    struct RequestHandler< RaycastRequest<CB> >
    {
        static void HandleRequest(
            RaycastRequest<CB>& request,
            DefaultDomain::Entity::Handle<components::World> world, 
            DefaultDomain::Entity object)
        {
            if(request.distance == 0)
            {
                //Print("0 distance raycast requested from " << object.id);
                return;
            }
            if(isnan(request.angle))
            {
                Print("ignoring NAN angled request from " << object.id);
                return;
            }
            auto body = object.search_parents<components::Body>();
            if(!body.is_valid()) return;
            //Print("raycast req " << request.distance << " from " << request.position);

            auto angle_vec = b2Vec2(cos(request.angle), sin(request.angle));
            b2Vec2 p2 = request.position +
                request.distance * angle_vec;

            auto cb = &request.cb;//const_cast<CB*>(&request.cb);
            world->world->RayCast(
                cb,
                request.position, 
                p2);
            cb->finish();

            

            if(world->draw_raycasts)
                world->raycasts.push_back({
                    request.position,
                    cb->shortest == -1 ? p2 : 
                        (request.position + (request.distance * cb->shortest * angle_vec))
                });
        }

    };



    template<typename Action, typename Controller>
    struct Sensor: InteractorOrgan<Action, Controller>
    {
        float& sensor_output()
        {
            return this->controller->parameter(1);
        }
    };

    template<
        typename Derived,
        template<typename> typename CallbackTpl,
        typename Controller = NeuronController<2>
    >// typename Callback>
    struct Raycaster: Sensor<
        RaycastAction< CallbackTpl< Derived > >,
        Controller >
    {
        void draw (sf::RenderTarget& target, DefaultDomain::Entity owner)
        {
        }
    };


    struct RaycastCallback : b2RayCastCallback
    {
        float shortest = -1;

        float ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction)
        {

            if(fraction < 0)
                return 1;

            if(shortest != -1 && fraction > shortest)
                return fraction;

            shortest = fraction;

            return fraction;
        }

    };

    template<typename O>
    struct SensorOutReport
    {
        void operator() (auto cb, Organ* o)
        {
            auto new_o = dynamic_cast<O*>(o);
            assert(new_o);
            new_o->controller->sensor_output() = cb->shortest;
        }
    };

    template<typename O>
    struct DistanceDetectCB: RaycastCallback
    {
        using Organ = O;
        Organ* o;

        DistanceDetectCB(Organ* o): o(o)
        {
        }

        void finish()
        {
            if(shortest != -1)
                o->sensor_output() = shortest;
        }


    };

    template<typename Controller = NeuronController<2>>
    struct DistanceSensor: Raycaster<
        DistanceSensor<Controller>,
        DistanceDetectCB,
        Controller                  >
    {};

}
}
