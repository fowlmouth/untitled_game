#pragma once

namespace fowl
{
namespace actuators
{



    struct ThrustUnit
    {
        static constexpr float energy_cost = 2.0; // energy cost to create 1 ThrustUnit
    };

    struct ThrustRequest
    {
        b2Vec2 position;
        float angle, units;

        ThrustRequest(b2Vec2 position, float angle, float units)
        : position(position), angle(angle), units(units)
        {
        }

        float cost() const
        {
            return ThrustUnit::energy_cost * units;
        }

    };


    template<>
    struct RequestHandler<ThrustRequest>
    {
        static void HandleRequest(
            const ThrustRequest& request,
            DefaultDomain::Entity::Handle<components::World> world, 
            DefaultDomain::Entity object)
        {
            if(request.units == 0) return;
            //turn request into thrust
            auto body = object.search_parents<components::Body>();
            if(!body.is_valid()) return;

            //Print(object.id << " thrust " << request.units);

            b2Vec2 force =
                request.units * b2Vec2( cos(request.angle), sin(request.angle) );
            
            //apply force
            body->body->ApplyForce(force, request.position, true);

        }
    };


    struct ThrusterAction
    {
        ThrustRequest
        activate (DefaultDomain::Entity obj, Organ* organ, auto controller) const
        {
            auto body = obj.get<components::Body>();
            assert(body);
            float magnitude = controller->parameter(0);
            // float turn = controller->parameter(1);
            // organ->turn(turn);
            float angle = organ->get_angle(obj);
            return ThrustRequest(
                organ->get_position(obj),
                angle,
                magnitude
            );
        }
    };

    template<typename Controller = NeuronController<2> >
    struct Thruster: InteractorOrgan< ThrusterAction, Controller >
    {
    };


}
}
