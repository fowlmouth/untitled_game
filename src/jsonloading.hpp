#pragma once

#include "json.hpp"

namespace fowl
{

using json = nlohmann::json;

template<typename From, typename To>
struct Read
{
	static void read(const From& , To& );
};


template<typename From, typename To>
struct Write
{
	static void act(To& , const From& );
};

using namespace entity;
template<>
struct Read<json, DefaultDomain::Entity>
{
	static void read(const json& from, DefaultDomain::Entity& to)
	{
		if(from.type() == json::value_t::object)
		{
			Print(from.dump());
		}
	}
};

using namespace components;
template<>
struct Read<json, DefaultDomain::Entity::Handle<Body>>
{
	static void read(const json& from, DefaultDomain::Entity::Handle<Body>& to)
	{
		if(from == "dynamic")
			to.create_dynamic(b2Vec2(0,0));
		else
			to.create_static();
	}
};

template<> struct Read<DefaultDomain::Entity::Handle<Body>, json>
{
	static void read(const DefaultDomain::Entity::Handle<Body>& from, json& to)
	{

	}
};

template<>
struct Read<DefaultDomain::Entity, json>
{
	static void read(const DefaultDomain::Entity& from, json& to)
	{

	}
};



}
