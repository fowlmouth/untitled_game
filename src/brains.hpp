
#pragma once
#include <algorithm>

namespace fowl
{
namespace neural
{

struct Neuron;

struct Lobe
{
    using Ptr = Lobe*;

    std::vector<Neuron*> neurons;


    virtual ~Lobe();
    virtual void step();

    Neuron* add(Neuron* neuron);
};

struct Neuron
{
    using Ptr = Neuron*;

    Lobe::Ptr lobe;

    Neuron(): lobe(nullptr)
    {}

    Neuron(Lobe::Ptr lobe): Neuron()
    {
        lobe->add(this);
    }

    virtual ~Neuron()
    {}
    virtual void step()
    {}



};

struct Brain
{
    std::vector<Lobe::Ptr> lobes;

    ~Brain()
    {
    }
    
    void step()
    {
        for(auto& lobe: lobes)
            lobe->step();
    }

    Lobe::Ptr add_lobe(Lobe::Ptr lobe)
    {
        lobes.push_back(lobe);
        return lobe;
    }
};


Lobe::~Lobe()
{
    for(Neuron* n: neurons)
        delete n;
}

Neuron::Ptr Lobe::add(Neuron::Ptr neuron)
{
    assert(! neuron->lobe);
    neurons.push_back(neuron);
    neuron->lobe = this;
    return neuron;
}


void Lobe::step()
{
    for(auto& n: neurons)
        n->step();
}



struct FloatNeuron: public Neuron
{
    using Ptr = FloatNeuron*;

    struct Synapse
    {
        using Ptr = Synapse*;
        FloatNeuron::Ptr a, b;
        float weight;

        float apply_weight()
        {
            return a->value * weight;
        }

        static Ptr connect(FloatNeuron::Ptr a, FloatNeuron::Ptr b, float weight = 1.0)
        {
            Ptr s = new Synapse(a, b, weight);
            a->outputs.push_back(s);
            b->inputs.push_back(s);
            return s;
        }
        ~Synapse()
        {
            auto idx = std::find(a->outputs.begin(), a->outputs.end(), this);
            if(idx != a->outputs.end()) a->outputs.erase(idx);
            idx = std::find(b->inputs.begin(), b->inputs.end(), this);
            if(idx != b->inputs.end()) b->inputs.erase(idx);
        }
    private:
        Synapse(FloatNeuron::Ptr a, FloatNeuron::Ptr b, float weight = 1.0)
        : a(a), b(b), weight(weight)
        {}

    };

    std::vector<Synapse::Ptr> inputs, outputs;
    float value = 0,
        decayRate = 0.9999888,
        bias = 0.0;

    void step()
    {
        if(! inputs.empty())
        {
            float sum = 0;
            for(auto& s: inputs)
                sum += s->apply_weight();
            value = bias + sum;
        }
        fire();
        decay();
    }

    void fire()
    {
    }

    void decay()
    {
        value *= decayRate;
    }


    FloatNeuron(Lobe::Ptr lobe)
    : Neuron(lobe)
    {}
    ~FloatNeuron()
    {
        while(! inputs.empty())
            delete inputs.back();
        while(! outputs.empty())
            delete outputs.back();
    }


    static Synapse::Ptr connect (Ptr a, Ptr b, float weight)
    {
        return Synapse::connect(a,b,weight);
    }

};

}
}