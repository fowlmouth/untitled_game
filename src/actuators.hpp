
#pragma once

#include <valarray>
#include "aux.hpp"

namespace fowl
{

//new types
// MountedPart - represents a physical part mounted to a shape
//    has some common properties: offset position, offset angle
//    connected with a lobe to control it
//
// example parts
//  thruster
//  sensor - raycast space and report distance to closest object
//     visual sensors would also report the object's Material as color
//  some kind of emitter?


using namespace entity;
using namespace neural;

namespace actuators
{




    struct Organ
    {
        virtual ~Organ() {}
        virtual void setup(DefaultDomain::Entity obj) {}
        virtual void step(DefaultDomain::Entity obj, float dt) = 0;
        virtual void draw(sf::RenderTarget& target, DefaultDomain::Entity owner)
        {
            float angle = get_angle(owner);
            auto p1 = sfVec(B2Ratio * get_position(owner));
            auto p2 = p1 + 5.f * sf::Vector2f( cos(angle), sin(angle) ) ;
            ::draw_line( target, p1, p2, sf::Color::White, 0.8 );

        }

        float angle = 0, turn_rate = M_PI / 64;
        b2Vec2 position;

        Organ (b2Vec2 position): position(position)
        {}
        Organ ()
        {}

        void turn (float amount)
        {
            angle += clamp(amount, -1.f, 1.f) * turn_rate;
        }

        b2Vec2 get_position (DefaultDomain::Entity owner)
        {
            auto body = owner.search_parents<components::Body>();
            auto& xf = body->body->GetTransform();
            return b2Mul(xf, position);
        }
        float get_angle (DefaultDomain::Entity owner)
        {
            auto body = owner.search_parents<components::Body>();
            return angle + body->body->GetAngle();
        }
    };
}

namespace components
{
    struct OrganGroup
    {
        std::vector<actuators::Organ*> organs;
    };
}
namespace entity
{
    DefineBehavior(components::OrganGroup)
    {
        actuators::Organ* newOrgan (actuators::Organ* organ)
        {
            this->derived()->organs.push_back(organ);
            organ->setup((DefaultDomain::Entity)this->derived());
            return organ;
        }

        b2Vec2 get_position (actuators::Organ* organ)
        {
            return organ->get_position((DefaultDomain::Entity)this->derived());
        }
    };
}


namespace actuators
{
    struct BrainOrgan : Organ
    {
        neural::Brain* brain = new neural::Brain;

        void step (DefaultDomain::Entity obj, float dt)
        {
            brain->step();
        }
        void draw(sf::RenderTarget& target, DefaultDomain::Entity owner)
        {
        }

    };


    template<typename R>
    struct RequestHandler
    {
        static void HandleRequest(
                R& request, 
                DefaultDomain::Entity::Handle<components::World> world, 
                DefaultDomain::Entity object
        ) = delete;
    };

    template<typename R>
    void ExecRequest(
            R& request, 
            DefaultDomain::Entity::Handle<components::World> world, 
            DefaultDomain::Entity object )
    {
        //take energy and call requesthandler<R>
        auto v = object.get<components::Vitality>();
        if(!v.is_valid())
            return;
        if(! v.take_energy(request.cost()))
            return;
        //make it so
        RequestHandler<R>::HandleRequest(request, world, object);
    }



    template<typename C>
    struct BaseOrgan: public Organ
    {
        using Controller = C;

        Controller* controller;
        BaseOrgan()
        {
            controller = new Controller;
        }
        ~BaseOrgan()
        {
            delete controller;
        }

        void setup(DefaultDomain::Entity obj)
        {
            controller->setup(obj);
        }

    };

    template<typename A, typename C>
    struct InteractorOrgan: public BaseOrgan<C>
    {
        //interacts with the world through a request driven by the controller
        //world requests cost the entity energy
        using Action = A;
        using Controller = C;

        void step (DefaultDomain::Entity obj, float dt)
        {
            auto world = obj.search_parents<components::World>();
            auto req = Action().activate(obj, this, this->controller);
            ExecRequest(
                req,
                world,
                obj
            );
        }

        float& magnitude()
        {
            return this->controller->parameter(0);
        }
    };






    struct InternalSensor: Organ
    {

    };



    template<unsigned N>
    struct NeuronController: Lobe
    {
        FloatNeuron* special[N];
        NeuronController (): Lobe()
        {
            for(int i = 0; i < N; ++i)
                special[i] = new FloatNeuron(this);
        }

        void setup (DefaultDomain::Entity obj)
        {
            auto og = obj.get<components::OrganGroup>();
            for(Organ* o: og->organs)
            {
                BrainOrgan* b = dynamic_cast<BrainOrgan*>(o);
                if(b)
                {
                    b->brain->add_lobe(this);
                    return;
                }
            }
            auto b = (BrainOrgan*)og.newOrgan(new BrainOrgan);
            b->brain->add_lobe(this);

        }

        inline float& parameter(unsigned index)
        {
            return special[index]->value;
        }
    };

    template<unsigned N>
    struct StaticController
    {
        float values[N];

        void setup (DefaultDomain::Entity obj)
        {}

        inline float& parameter(unsigned index)
        {
            return values[index];
        }
    };

    //use for Thruster, Raycaster etc
    template<template<typename>typename Tpl, unsigned N = 2>
    using StaticControlled = Tpl<StaticController<N>>;



}

namespace systems
{

    struct BehaviorSystem: DefaultDomain::System< components::OrganGroup >
    {
        void update(float dt)
        {
            entities().each([&](DefaultDomain::Entity entity, components::OrganGroup& body){
                for(auto o: body.organs)
                {
                    o->step(entity, dt);
                }
            });
        }
    };

}
}

#include "actuators/raycaster.hpp"
#include "actuators/thruster.hpp"
#include "actuators/turning_muscle.hpp"
