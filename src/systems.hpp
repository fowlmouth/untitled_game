#pragma once
#include <set>


#include "actuators.hpp"

namespace fowl
{
namespace systems
{

using namespace components;
using namespace entity;

template<typename...X>
using System = DefaultDomain::System<X...>;

struct DeletionSystem: public System<>
{
    std::set<EntityID> queue;

    void destroy (EntityID entity)
    {
        queue.insert(entity);
    }

    void empty_queue()
    {
        for(auto e: queue)
            em->destroy_entity(e);
        queue.clear();
    }

    void update(float dt)
    {
        empty_queue();
    }
};

struct PhysicsSystem: public System<World>
{
    void update(float dt)
    {
        entities().each([dt](DefaultDomain::Entity e, World& w){
            w.world->Step(dt, 8, 4);
            w.world->ClearForces();
        });
    }
};



struct PlayerInputSystem: public System<>
{
	EntityID player = 0;
    EntityID camera = 0;

    void update(float dt)
    {
        if(!player)
        {
            move_camera(dt);
        }
    }

    void move_camera(float dt)
    {
        if(camera)
        {
            if(! em->is_valid(camera))
            {
                camera = 0;
                return;
            }
            auto cam = em->get_component<OrthogonalCamera>(camera);
            if(cam.get())
            {
                using Key = sf::Keyboard::Key;

                sf::Vector2f move;
                const auto MoveSpeed = 500.0 * dt;

                if(sf::Keyboard::isKeyPressed(Key::Right))
                    move.x += MoveSpeed;
                else if(sf::Keyboard::isKeyPressed(Key::Left))
                    move.x -= MoveSpeed;
                if(sf::Keyboard::isKeyPressed(Key::Down))
                    move.y += MoveSpeed;
                else if(sf::Keyboard::isKeyPressed(Key::Up))
                    move.y -= MoveSpeed;

                cam->position += move;
            }
        }

        
    }

};


}
}

#include "systems/rendersystem.hpp"
#include "systems/spawnsystem.hpp"

