#pragma once


namespace fowl
{
namespace systems
{

using namespace components;
using namespace entity;

struct RenderSystem: public System<Shape>
{
    sf::RenderTarget& target;
    sf::Color clear_color;
    bool draw_energy_bars = false;

    RenderSystem(sf::RenderTarget& target)
    : target(target), clear_color(sf::Color::Black)
    {}

    void update(float dt)
    {
    }

    struct b2draw: b2QueryCallback
    {
        RenderSystem& r;
        int drawn = 0;

        b2draw(RenderSystem& r): r(r)
        {}
        bool ReportFixture (b2Fixture* fixture)
        {

            b2Shape* shape = fixture->GetShape();
            b2Body* body = fixture->GetBody();
            EntityID obj = (EntityID)fixture->GetUserData();
            //PrintCode(obj);
            const auto& xf = body->GetTransform();

            auto mat = r.em->get_component<Material>(obj);
            sf::Color draw_color = 
                mat.is_valid() ? mat->fill_color : sf::Color::White;
            auto og = r.em->get_component<OrganGroup>(obj);

            switch(shape->GetType())
            {
            case b2Shape::e_polygon:
            {
                b2PolygonShape* poly = (b2PolygonShape*)shape;
                sf::ConvexShape polygon( poly->GetVertexCount() );
                for(int i = 0; i < poly->GetVertexCount(); ++i)
                {
                    //auto v = B2Ratio * (poly->GetVertex(i) + body->GetPosition());
                    //polygon.setPoint(i, sf::Vector2f(v.x, v.y));
                    b2Vec2 p = b2Mul( xf, poly->GetVertex(i) );
                    polygon.setPoint(i, sfVec(B2Ratio * p));
                }
                polygon.setOutlineThickness(0);
                polygon.setFillColor(draw_color);

                r.target.draw(polygon);
                if(og.is_valid())
                    for(auto o: og->organs)
                        o->draw(r.target, r.em->get(obj));
                drawn++;
                
                break;
            }
            case b2Shape::e_edge:
            {
                b2EdgeShape* edge = (b2EdgeShape*)shape;
                sf::Vertex line[]{
                    sf::Vertex(B2Ratio * sfVec(edge->m_vertex1 + body->GetPosition()), draw_color),
                    sf::Vertex(B2Ratio * sfVec(edge->m_vertex2 + body->GetPosition()), draw_color)
                };
                r.target.draw(line, 2, sf::Lines);
                drawn++;

                break;
            }
            case b2Shape::e_circle:
            {
                b2CircleShape* c = (b2CircleShape*)shape;
                sf::CircleShape circle;
                float sfRadius = c->m_radius * B2Ratio;
                circle.setRadius(sfRadius);
                circle.setFillColor(draw_color);
                auto p = B2Ratio * b2Mul(xf, c->m_p);
                //auto p = B2Ratio * (body->GetPosition() + c->m_p);
                circle.setPosition(p.x, p.y);
                circle.setOrigin(sf::Vector2f(sfRadius,sfRadius));
                r.target.draw(circle);
                drawn++;

                break;
            }
            default:
                std::cout << "unhandled " << shape->GetType() << std::endl;

            }

            if(r.draw_energy_bars)
            {
                auto vit = r.em->get_component<components::Vitality>(obj);
                if(vit.is_valid())
                {
                    float energy_pct = vit->energy.current / vit->energy.max;
                    sf::RectangleShape rect;
                    rect.setSize(sf::Vector2f(30.0 * energy_pct, 5.0));
                    rect.setPosition(sfVec(B2Ratio * body->GetPosition()));
                    rect.setFillColor(sf::Color::Blue);
                    r.target.draw(rect);
                }
            }

            return true;
        }
    };

    void draw_scene(DefaultDomain::Entity scene, DefaultDomain::Entity camera)
    {
        target.clear(clear_color);

        auto world = scene.get<World>();
        auto cam = camera.get<OrthogonalCamera>();
        cam->update_target(em);

        sf::View view(cam->position, cam->size);
        view.zoom(1/cam->zoom);

        auto bb = cam->aabb();
        auto cb = b2draw(*this);

        auto old_view = target.getView();
        target.setView(view);
        world->world->QueryAABB(&cb, bb);
        if(world->draw_raycasts)
        {

            sf::Vertex line[2];
            sf::Color color = sf::Color::Red;
            color.a /= 4;

            for(auto& r: world->raycasts)
            {

                //Print( r.a << " to " << r.b );
                // line[0] = sf::Vertex(B2Ratio * sfVec(r.a), color);
                // line[1] = sf::Vertex(B2Ratio * sfVec(r.b), color);
                sf::LineShape shape(B2Ratio * sfVec(r.a), B2Ratio * sfVec(r.b));
                shape.setFillColor(color);
                shape.setThickness(1);
                target.draw(shape);
                //target.draw(line, 2, sf::Lines);
            }
            //world->raycasts.clear();

        }
        target.setView(old_view);


    }
};

}
}

