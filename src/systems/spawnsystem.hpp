#pragma once

#include "jsonloading.hpp"

namespace fowl
{
namespace systems
{


struct SpawnSystem: public System<components::SpawnRegion>
{

    void update(float dt)
    {
        entities().each([&](DefaultDomain::Entity obj, components::SpawnRegion& r)
        {
            auto p = r.get_b2position();

            auto& tmpl = r.tmpl;
            const int max = 5;

            for(int created = 0;
                r.create > 0 && created < max;
                --r.create, ++created )
            {
                //tmpl.create(em);
            }

        });
    }

};

}
}
