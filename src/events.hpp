
#pragma once

#include <boost/variant.hpp>
#include <map>
#include <algorithm>

namespace fowl
{


using EventID = int;
struct Event
{
	using Value = boost::variant<int, float>;
	EventID id;
	std::map<int, Value> parameters;

};

struct EventSubscriber
{
	using Callback = std::function<bool(const Event&)>;

	std::map<int, std::vector<Callback>> handlers;
	inline void subscribe (int event, const Callback& callback)
	{
		handlers[event].push_back(callback);
	}

	inline void emit (const Event& event) const
	{
		try
		{
			const auto& cbs = handlers.at(event.id);
			for(const auto h: cbs) h(event);
		}
		catch(const std::out_of_range&)
		{
		}
	}

};

struct EventSource
{
	std::vector<EventSubscriber*> subs;

	inline void emit (const Event& event) const
	{
		for(const auto s: subs) s->emit(event);
	}

	inline void add (EventSubscriber* sub)
	{
		auto pos = std::lower_bound(subs.begin(), subs.end(), sub);
		if(pos == subs.end() || *pos != sub) subs.insert(pos, sub);
	}
	inline void remove(EventSubscriber* sub)
	{
		auto pos = std::lower_bound(subs.begin(), subs.end(), sub);
		if(pos != subs.end() && *pos == sub) subs.erase(pos); 
	}

};

namespace Events
{
	enum
	{
		Update
	};

	namespace update
	{
		enum{ dt };
	}
}

}

