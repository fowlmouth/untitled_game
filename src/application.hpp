#pragma once

#define TAU (M_PI*2)

namespace fowl
{

void create_border_segment (entity::DefaultDomain::Entity static_body, 
    b2Vec2 p1, b2Vec2 p2, const components::Material& mat, float friction = 0.5, float restitution = 0.1)
{
    auto border = static_body.create_child();
    auto shape = border.create_component<components::Shape>();
    shape.create_edge(p1, p2, friction, restitution);
    *border.create_component<components::Material>() = mat;

}

template<typename Container>
void create_borders (entity::DefaultDomain::Entity static_body, const Container& points, const components::Material& mat)
{
    for(int i = 0; i < points.size(); ++i)
        create_border_segment(
            static_body,
            points[i], points[(i+1)%points.size()],
            mat
        );
}

void create_borders (entity::DefaultDomain::Entity static_body, float W, float H, const components::Material& mat)
{
    std::vector<b2Vec2> points {
        b2Vec2(0,0),
        b2Vec2(W/B2Ratio,0),
        b2Vec2(W/B2Ratio, H/B2Ratio),
        b2Vec2(0,H/B2Ratio)      };
    create_borders(static_body, points, mat);
}

template<typename Container>
void radial_points (
    int num_points, Container& output, 
    float magnitude = 1, float angle_offset = 0)
{
    output.resize(num_points);
    for(int i = 0; i < num_points; ++i)
    {
        float angle = angle_offset + ((float)i / (float)num_points * TAU);
        output[i] = magnitude * b2Vec2(cos(angle), sin(angle));//sin(angle), cos(angle));
    }
}

auto player_ship (entity::DefaultDomain::Entity parent)
{
    using namespace fowl::components;
    using namespace fowl::actuators;

    auto ship = parent.create_child();
    auto body = ship.create<Body>();
    body.create_dynamic(b2Vec2());
    auto shape = ship.create<Shape>();

    std::vector<b2Vec2> points;
    radial_points(3, points, 20/B2Ratio);

    shape.create_from_points(points, 0.5, 0.3);

    auto mat = ship.create<Material>();
    mat->fill_color = sf::Color::Green;
    mat->outline_color = sf::Color::Green;

    auto og = ship.create<OrganGroup>();
    for(int i = 1; i < 3; ++i)
    {
        //create forward thrusters
        auto thruster = new StaticControlled<Thruster>;
        thruster->position = points[i];
        thruster->angle = M_PI;
        og.newOrgan(thruster);
        Print(points[i]);
    }
    {
        Print(points[0]);
        auto thruster = new StaticControlled<Thruster>;
        thruster->position = points[0];
        thruster->angle = M_PI / 2;
        og.newOrgan(thruster);
        thruster = new StaticControlled<Thruster>;
        thruster->position = points[0];
        thruster->angle = -M_PI/ 2;
        og.newOrgan(thruster);
        auto rc = new StaticControlled<DistanceSensor>;
    }



    return ship;
}


struct PreparedScene
{
    entity::DefaultDomain::Entity scene, camera;
};

struct TestScene2
{
    PreparedScene create (entity::DefaultDomain::EntityManager* em) const
    {
        auto scene = em->create_entity(0);
        auto world = scene.create_component<components::World>();
        world->draw_raycasts = true;

        auto camera = scene.create_child();
        auto cam = camera.create_component<components::OrthogonalCamera>();
        cam->position = sf::Vector2f(0,0);//50,50);

        auto result = PreparedScene{scene, camera};

        auto static_body = scene.create_child();
        static_body.create<components::Body>().create_static();


        std::vector<b2Vec2> points;
        {
            const int num_points = 4;
            radial_points(num_points, points, 10, TAU/8);
            components::Material mat;
            mat.fill_color = sf::Color::White;
            mat.fill_color.a = 100;
            create_borders(static_body, points, mat);
        }

        // Entity obj = scene.create_child();
        // obj.create<Body>().create_dynamic(b2Vec2(0,0));
        // obj.create<Shape>().create_circle(1, b2Vec2(0,0), 0.5, 0.5);
        // cam->track(obj);


        auto ship = player_ship(scene);



        return result;

    }
};

struct TestScene
{
    PreparedScene create(entity::DefaultDomain::EntityManager* em) const
    {
        using namespace components;

        int w = 600, h = 400;

        auto scene = em->create_entity(0);
        auto world = scene.create_component<components::World>();
        world->draw_raycasts = true;

        auto camera = scene.create_child();
        auto cam = camera.create_component<components::OrthogonalCamera>();
        cam->position = sf::Vector2f(50,50);


        auto static_body = scene.create_child();
        auto body = static_body.create_component<components::Body>();
        body.create_static();

        {
            components::Material mat;
            mat.fill_color.a = 160;
            create_borders(static_body, w, h, mat);
        }

        auto spawn_region = scene.create_child();
        auto region = spawn_region.create_component<components::SpawnRegion>();
        region->upper_left = sf::Vector2f(3,3);
        region->size = sf::Vector2f(2,2);


        auto obj = scene.create_child();
        body = obj.create< Body>();
        body.create_dynamic(b2Vec2(8,8));
        body->body->SetTransform(b2Vec2(2,8), 0);
        //obj.create_component<components::Shape>().create_radial(40, 6, 0.3);
        obj.create< Shape>().create_circle(30, b2Vec2(0,0), 0.1);
        auto mat = obj.create_component< components::Material>();
        auto color = &mat->fill_color;
        color->r = 40;
        color->g = 40;
        color->b = 250;
        color->a = 255.0/3;
        auto vit = obj.create<components::Vitality>();
        vit->energy.max = 1000;
        vit->energy.current = 90000;
        auto og = obj.create<components::OrganGroup>();
        og.newOrgan(new actuators::BrainOrgan);
        auto thruster = new actuators::Thruster<>;
        //thruster->controller->parameter(0) = 0.2;
        og.newOrgan(thruster);
        auto sensor = new actuators::DistanceSensor<>;
        //sensor->controller->parameter(0) = 10;
        og.newOrgan(sensor);


        //auto brain = obj.create_component< components::Brain>();
        //auto sensor = (Sensor*)brain->add_lobe(new Sensor);
        //sensor->magnitude() = 5.0;

        {
            auto o2 = obj.create_child();
            o2.create_component<components::Shape>().create_circle(30, 1/B2Ratio * b2Vec2(-45,0), 0.1);
            auto mat2 = o2.create_component<components::Material>();
            *mat2 = *mat;



        }

        sf::Color c;
        c.a = 255.0 / 3;
        c.r = 20;
        c.g = 20;
        c.b = 230;

        std::uniform_int_distribution<> dis(3, 6);
        std::uniform_real_distribution<> flt(0, 1);
        for(int i = 0; i < 7; ++i)
        {
            auto pos = b2Vec2( 
                (float)w / B2Ratio * flt(gen), 
                (float)h / B2Ratio * flt(gen) );
            auto angle = flt(gen) * TAU;
            auto points = dis(gen);

            Print(pos << " " << points);
            auto o = scene.create_child();
            auto body = o.create<Body>();
            body.create_dynamic(b2Vec2(0,0));
            body->body->SetTransform(pos, angle);

            o.create<Shape>().//create_circle(20, b2Vec2(), 0.1);
            create_radial(20, points, 0.1);

            auto mat = o.create_component<Material>();
            mat->fill_color = c;

            og = o.create<OrganGroup>();
            og.newOrgan(new actuators::BrainOrgan);

            // auto sensor = new actuators::DistanceSensor;
            // sensor->controller->parameter(0) = 10;
            // sensor->position = b2Vec2(-25.0 / B2Ratio, 0);
            // og.newOrgan(sensor);

            vit = o.create<components::Vitality>();
            vit->energy = {700,500};

        }

        return PreparedScene{ scene, camera };

    }
};

struct Racetrack
{
    int num_points = 32;
    float track_radius = 13;
    float track_width = 3.5;

    PreparedScene create(entity::DefaultDomain::EntityManager* em) const
    {

        using namespace components;
        using namespace actuators;

        auto scene = em->create_entity(0);
        auto world = scene.create<World>();
        world->draw_raycasts = true;

        auto static_body = scene.create_child();
        auto body = static_body.create_component<Body>();
        body.create_static();

        std::valarray<b2Vec2> points;
        radial_points(num_points, points);
        auto inner_mag = track_radius - track_width/2.0f;
        auto outer_mag = track_radius + track_width/2.0f;

        for(int i = 0; i < num_points; ++i)
        {
            auto p1 = points[i];
            auto p2 = points[(i+1) % num_points];
            Material mat;
            mat.fill_color.a = 160;
            create_border_segment(static_body,
                inner_mag * p1, inner_mag * p2, mat);
            create_border_segment(static_body,
                outer_mag * p1, outer_mag * p2, mat);
        }

        auto camera = scene.create_child();
        auto cam = camera.create_component<OrthogonalCamera>();
        cam->position = sfVec(B2Ratio * track_radius * points[0]);
        cam->zoom = 2;


        auto bot = scene.create_child();
        body = bot.create<Body>();
        body.create_dynamic(track_radius * points[0]);
        body->body->SetTransform(body->body->GetWorldCenter(), -M_PI / 2.0);
        auto shape = bot.create<Shape>();

        {
            auto w = .8f, h = .5f;
            points = {
                b2Vec2( w/2,  h/2 - 0.1 ),
                b2Vec2( w/2, -h/2 + 0.1 ),
                b2Vec2(-w/2, -h/2 ),
                b2Vec2(-w/2,  h/2 )
            };
            b2PolygonShape polygonShape;
            polygonShape.Set(&points[0], 4);
            b2FixtureDef fd;
            fd.density = 0.4;
            fd.friction = 0.4;
            fd.restitution = 0.1;
            fd.shape = &polygonShape;
            shape.create_fixture(fd);

            auto mat = bot.create<Material>();
            mat->fill_color = sf::Color::Green;
            mat->fill_color.r += 20;
            mat->fill_color.g += 20;

            auto vit = bot.create<Vitality>();
            vit->energy = {90000,100};
            auto og = bot.create<OrganGroup>();
            og.newOrgan(new BrainOrgan);
            for(int i = 0; i < 4;++i)
            {
                auto thruster = new Thruster<>;
                auto pos = points[i];
                thruster->position = pos;
                thruster->angle = 0;//M_PI;
                thruster->magnitude() = 0.08;
                og.newOrgan(thruster);


                if(i < 2)
                {
                    const auto offs = (i == 0) ? 1.f : -1.f;
                    const auto turn = TAU / 16 * offs;
                    for(int j = 0; j < 3; ++j)
                    {
                        auto sensor = new DistanceSensor<>;
                        sensor->magnitude() = 5 - j;
                        sensor->position = pos;
                        sensor->angle = (float)j * turn;
                        og.newOrgan(sensor);
                    }
                }

            }
        }



        return PreparedScene{scene, camera};
    }
};


struct App
{
    sf::RenderWindow& window;
    systems::RenderSystem* render_system;
    systems::PlayerInputSystem* player_input_system;
    entity::DefaultDomain::EntityManager* em;
    entity::DefaultDomain::Entity scene, camera;

    App(sf::RenderWindow& w)
    : window(w)
    {
        using namespace fowl;
        using namespace entity;

        em = new entity::DefaultDomain::EntityManager;
        render_system = new systems::RenderSystem(window);
        player_input_system = new systems::PlayerInputSystem();

        em->add_system(player_input_system);
        em->add_system(new systems::SpawnSystem());
        em->add_system(new systems::BehaviorSystem());
        em->add_system(new systems::PhysicsSystem());
        em->add_system(render_system);
    }

    template<typename P>
    void setup (const P& creator)
    {
        auto _ = creator.create(em);
        scene  = _.scene;
        camera = _.camera;

        auto cam = camera.get<components::OrthogonalCamera>();
        assert(cam.is_valid());

        auto screen_size = window.getSize();
        cam->size.x = screen_size.x;
        cam->size.y = screen_size.y;
        player_input_system->camera = camera;
    }

    void handle_event(sf::Event& event)
    {
        switch(event.type)
        {
        case sf::Event::Closed:
            if(paused) window.close();
            else       paused = true;
            break;

        case sf::Event::KeyPressed:
            if(event.key.code == sf::Keyboard::Key::Space)
                paused = ! paused;
            break;

        }
    }

    bool paused = false;
    void run()
    {
        using namespace fowl;

        sf::Event event;
        sf::Clock clock;
        float ang = 0;
        while(window.isOpen())
        {
            while(window.pollEvent(event))
                handle_event(event);

            auto dt = clock.restart().asSeconds();
            if(!paused)
                em->update_all(dt);
            fowl::Event update_event;
            update_event.id = Events::Update;
            update_event.parameters[Events::update::dt] = dt;
            em->event_source.emit(update_event);

            render_system->draw_scene(scene, camera);


            window.display();


            if(!paused)
                scene.get<World>()->raycasts.clear();



        }
    }
};
}