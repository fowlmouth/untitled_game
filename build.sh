#!/bin/sh

g++ -std=c++1z -g -O0 -Isrc \
  src/main.cpp -o game  \
  -lsfml-graphics -lsfml-window -lsfml-system \
  -lBox2D \
  -fpermissive 
